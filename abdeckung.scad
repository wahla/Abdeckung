h_z = 30;
b_x = 10;
t_y = 5;

w = 1; //Wandstärke
wr = 1.5; //Rückwandstärke

difference() {
    cube([b_x,t_y,h_z]);
    translate([w,0,w]) cube([b_x-(w*2),t_y-wr,h_z-w*2]);
}